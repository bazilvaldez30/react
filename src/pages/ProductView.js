import { useEffect, useState } from "react"
import { Container, Row, Col, Card, Button, Image } from "react-bootstrap"
import { Link, useParams } from "react-router-dom"
import { useAddToCart } from "../hooks/useAddToCart"
import { useAuthContext } from "../hooks/useAuthContext"
import { useBuyNow } from "../hooks/useBuyNow"
import { formatCurrency } from "../utilities/formatCurrency"

export default function ProductView() {

  const [ productName, setProductName ] = useState("");
  const [ description, setDescription ] = useState("");
  const [ stocks, setStocks ] = useState("");
  const [ price, setPrice ] = useState("");
  const [ quantity, setQuantity] = useState(1);
  const [ path, setPath ] = useState()

  const { productId } = useParams()
  const { user } = useAuthContext()

  const { addToCart } = useAddToCart()
  const { buyNow } = useBuyNow()

  useEffect(() => {

    getProduct()

  }, [productId])

  const getProduct = async () => {

    const response = await fetch(`${process.env.REACT_APP_URI}/products/specificProduct/${productId}`)
    const json = await response.json()
    console.log(json)
    if(response.ok){
      setProductName(json.name);
      setDescription(json.description);
      setStocks(json.stocks);
      setPrice(json.price);
      setPath(json.path)
      console.log(json)
    }
  }


  return (
    <Container className="mt-5">
      <Row>
        <Col lg = {{span: 12}}>
          <Card>
            {stocks === 0 && <div className="error text-center display-5">OUT OF STOCK</div>}
            <Card.Body className="product-view text-center d-flex align-items-center">
              <div className="col-5">
                <Image src={`../${path}`} alt="..." className="img-fluid d-block mx-auto mb-3"></Image>
              </div>
              <div className="col-7 d-flex flex-column align-items-center justify-content-center p-5">
                <Card.Title className="mb-4 display-6">{productName}</Card.Title>
                <Card.Subtitle className="mb-2">Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{formatCurrency(price)}</Card.Text>
                <Card.Subtitle>Stocks Available</Card.Subtitle>
                <Card.Text>{stocks}</Card.Text>
                <div className="d-flex flex-column text-center">
                  <div className="mb-2">
                  <Button className="plus-minus-btn p-1" onClick={() => { if(quantity===1){return null}setQuantity(quantity-1) }}>-</Button>
                    <span className="ml-2 mr-2">{quantity}</span>
                  <Button className="plus-minus-btn p-1" onClick={() => { if(quantity === stocks){return null}setQuantity(quantity+1) }}>+</Button>
                  </div>
                  
                </div>
                { user ? 
                  <>
                  <Button disabled={quantity > stocks} onClick={() => {buyNow(productId, quantity); setStocks(stocks-quantity);}} className="product-view-btn mb-2 w-25" variant="primary" >Buy now</Button>
                  <Button onClick={() => addToCart(productId, quantity)} className="product-view-btn w-25" variant="primary" >Add to cart</Button>
                  </>
                  :
                  <>
                  <Button as={Link} to="/login" onClick={() => {buyNow(productId, quantity); setStocks(stocks-quantity);}} className="product-view-btn mb-2 w-25" variant="primary" >Buy now</Button>
                  <Button as={Link} to="/login" onClick={() => addToCart(productId) } className="product-view-btn w-25" variant="primary" >Add to cart</Button>
                  </>
                  
                }
              </div>
            </Card.Body>
					</Card>
        </Col>
      </Row>
    </Container>
  )

}