import { useState } from "react"
import { useProductsContext } from "../hooks/useProductsContext"
import { useAuthContext } from "../hooks/useAuthContext"
import { Form, Button, Container, Row, Col } from "react-bootstrap"
import Swal from 'sweetalert2'

const AddProduct = () => {
    const { dispatch } = useProductsContext()
    const { user } = useAuthContext()

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [stocks, setStocks] = useState('')
    const [price, setPrice] = useState('')
    const [path, setPath] = useState('')
    const [error, setError] = useState(null)
    const [emptyFields, setEmptyFields] = useState([])

    const handleSubmit = async (e) => {
        e.preventDefault()

        if (!user) {
            return setError('You must be logged in')
            
        }
        if(!user.isAdmin){
            return setError('Only admin have access to this action!')
            
        }

        const product = {name, description, stocks, price, path}

        const response = await fetch(`${process.env.REACT_APP_URI}/products/addProducts`, {
            method: 'POST',
            body: JSON.stringify(product),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.token}`
            }
        })
        const json = await response.json()

        if (!response.ok){
            setError(json.error)
            setEmptyFields(json.emptyFields)
        }
        if (response.ok) {
            setName('')
            setDescription('')
            setStocks('')
            setPrice('')
            setPath('')
            setError(null)
            setEmptyFields([])
            console.log('new workout added', json)
            dispatch({type: 'CREATE_PRODUCT', payload: json})
            Swal.fire({
                title: "Added Product Successful!",
                text: "New products has been added!",
                icon: "success",
                timer: 2000
            })
        }
    }

    return (
    <div className="add-product-div h-100 d-flex align-items-center justify-content-center">
        <Container>
            <Row>
                <Col lg = {{span: 12}}>
                    <Form className="addProducts bg-light" onSubmit={handleSubmit}>
                        <h3>Post New Products</h3>
                        <Form.Group className="mt-3">
                        <Form.Label>Product Name:</Form.Label>
                        <Form.Control
                            type="text"
                            onChange={(e) => setName(e.target.value)}
                            value={name}
                            className={emptyFields?.includes('name') ? 'error' : ''}
                        />
                        </Form.Group>

                        <Form.Group className="mt-2">
                        <Form.Label>Description:</Form.Label>
                        <Form.Control
                            type="text"
                            onChange={(e) => setDescription(e.target.value)}
                            value={description}
                            className={emptyFields?.includes('description') ? 'error' : ''}
                        />
                        </Form.Group>

                        <Form.Group className="mt-2">
                        <Form.Label>Stocks:</Form.Label>
                        <Form.Control
                            type="number"
                            onChange={(e) => setStocks(e.target.value)}
                            value={stocks}
                            className={emptyFields?.includes('stocks') ? 'error' : ''}
                        />
                        </Form.Group>

                        <Form.Group className="mt-2">
                        <Form.Label>Price:</Form.Label>
                        <Form.Control
                            type="number"
                            onChange={(e) => setPrice(e.target.value)}
                            value={price}
                            className={emptyFields?.includes('price') ? 'error' : ''}
                        />
                        </Form.Group>

                        <Form.Group className="mt-3">
                        <Form.Label>Image</Form.Label>
                        <Form.Control
                            type="text"
                            onChange={(e) => setPath(e.target.value)}
                            value={path}
                            className={emptyFields?.includes('path') ? 'error' : ''}
                        />
                        </Form.Group>

                        <Button className="mt-3 btn-dark btn-block p-2" type="submit">Add Product</Button>
                        {error && <div className="error">{error}</div>}
                    </Form>
                </Col>
            </Row>
        </Container>
    </div>
    )
}

export default AddProduct