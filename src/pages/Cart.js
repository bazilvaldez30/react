import React, { useEffect, useState } from 'react';
import { Stack, Offcanvas, Button, Container, CloseButton } from 'react-bootstrap';
import CartItem from '../components/CartItem';
import { useCheckOut } from '../hooks/useCheckOut';
import { useShoppingCart } from '../hooks/useShoppingCartContext';
import { formatCurrency } from '../utilities/formatCurrency';


const Cart = () => {
  
  const { show, handleClose, items, totalAmmount } = useShoppingCart()
  // const [userCartItems, setUserCartItems] = useState([])
  const { checkOut } = useCheckOut()

  // useEffect(() => {
  
  //     setUserCartItems(items.map(product => {
  //         return (
  //             <CartItem product={product} key={product.productId}/>
  //         )
  //     }))
    
  //   console.log(items)
  // }, [items])


  return (
    
      <Offcanvas className="text-bg-dark" show={show} placement="end" onHide={()=> handleClose()}>
        <Offcanvas.Header closeButton className='ml-4 mr-2 btn-close-white text-dark'>
          <Offcanvas.Title className='off-canvas display-6 p3 w-100'>Cart</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body className='off-canvas-body'>
          <Stack gap={3}>
            { items && items.map(product => (
              <CartItem product={product} key={product.productId}/>
            ))}
            { items.length === 0 && (
              <p className='text-center display-6 mt-5 text-muted'>Cart is empty</p>
            )}
            { items.length > 0 && (
              <>
              <div className='cart-total d-flex justify-content-between'>
                <span>
                  Total
                </span>
                <span> 
                  {formatCurrency(totalAmmount)}
                </span>
              </div>
              <Button onClick={() => {checkOut()}} variant='primary'>Check Out</Button>
              </>
            )
            }
          </Stack>
        </Offcanvas.Body>
      </Offcanvas> 
    
  ) 

}

export default Cart