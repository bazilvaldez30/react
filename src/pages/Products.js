import { useEffect, useState } from "react"
import { useProductsContext } from "../hooks/useProductsContext"

import ProductDetails from "../components/productsDetails"
import { useAuthContext } from "../hooks/useAuthContext"


const Products = () => {

    const {products, dispatch} = useProductsContext()
    const { user } = useAuthContext()

    useEffect(() => {

        if(user && user.isAdmin){
            const fetchProducts = async () => {

                const response = await fetch(`${process.env.REACT_APP_URI}/products/allProducts`, {
                    headers: {
                        "Content-Type": "application/json",
                        "authorization": `Bearer ${user.token}`
                    }
                })
                const json = await response.json()

                if (response.ok) {
                    dispatch({type: 'SET_PRODUCTS', payload: json})
                }
            }
            fetchProducts()
        }
            
       else{

            const fetchProducts = async () => {

                const response = await fetch(`${process.env.REACT_APP_URI}/products/allActiveProducts`)
                const json = await response.json()

                if (response.ok) {
                    dispatch({type: 'SET_PRODUCTS', payload: json})
                }
            }

            fetchProducts()
        }       
    }, [dispatch, user])

    const [query, setQuery] = useState("")
    let filteredProducts
    
    if(products){
        filteredProducts = products.filter(product => {
        return product.name.toLowerCase().includes(query.toLowerCase())
    })
    }
    
    return (
        <div className="ml-5 mr-5 py-5">
  
            <header className="text-center mb-5">
                <h1 className="display-6 font-weight-bold">ALL PRODUCTS</h1>
            </header>
            {/* <button className="d-flex align-items-center mb-4 rounded-pill">
                <span className="material-symbols-outlined p-2 pl-3 pr-2">tune</span><span className="p-2 mr-2">Filter</span>
            </button> */}
            <div className="d-flex ml-0 mb-3">
                <input className="col-md-2 col-12" value={query} onChange={e => setQuery(e.target.value)} type="search" placeholder="Search Products..." />
                <span className="material-symbols-outlined border p-2">
                    search
                </span>
            </div>
            <div className="row">
                {products && filteredProducts.map((product) => (
                    <ProductDetails product={product} key={product._id} />
                ))}
            </div>

        </div>
    )
}

export default Products



    

