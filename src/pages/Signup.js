import SignupForm from "../components/signupForm"


const Signup = () => {
    return (
        <div className="d-flex justify-content-center align-items-center">
            <div className="signup d-flex justify-content-center align-items-center" style={{width: "500px"}}>
                <SignupForm />
            </div>
        </div>
    )
}

export default Signup