
import LoginForm  from '../components/LoginForm'

const Login = () => {
    return (
        <div className="d-flex justify-content-center align-items-center">
            <div className="login border" style={{width: "500px"}}>
                <LoginForm />
            </div>
        </div>

    )
}

export default Login