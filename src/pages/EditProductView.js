import { useEffect, useState } from "react"
import { Container, Form, Row, Col, Button, Image } from 'react-bootstrap'
import { useParams, useNavigate } from "react-router-dom"
import { useAuthContext } from "../hooks/useAuthContext"
import Swal from 'sweetalert2'


export default function EditProductView() {

  const [ productName, setProductName ] = useState("")
  const [ description, setDescription ] = useState("")
  const [ stocks, setStocks ] = useState("")
  const [ price, setPrice ] = useState("")
  const [ path, setPath ] = useState("")
 

  const { user } = useAuthContext()
  const { productId } = useParams()
  
  
  useEffect(() => {
     getProduct()
  },[productId])

  const getProduct = async () => {
    const response = await fetch(`${process.env.REACT_APP_URI}/products/specificProduct/${productId}`)
    const json = await response.json()
    console.log(json)
    if(response.ok){
      setProductName(json.name);
      setDescription(json.description);
      setStocks(json.stocks);
      setPrice(json.price);
      setPath(json.path);
    }
  }

  const handleUpdateProduct = async (e) => {
    e.preventDefault()
    const name = productName;
    const response = await fetch(`${process.env.REACT_APP_URI}/products/updateProducts/${productId}`, {
      method: 'PATCH',
      body: JSON.stringify({name: name, description, stocks, price}),
      headers: {
        "Content-Type": "application/json",
        "authorization": `Bearer ${user.token}`
      }
      
    })

    const json = await response.json()

    console.log(json)
    if(response.ok){
      Swal.fire({
        title: "Product Updated Successfully!",
        text: "Nice one!",
        icon: "success",
        timer: 2000
    })
    }
    if(!response.ok){
      console.log(json.error)
    }
  }

  return (
    <Container className="mt-5">
      <div className="text-center mb-5">
        <h1>Edit Products</h1>
      </div>
      <Row>
        <Col lg = {{span: 10, offset: 1}}>
          <Form onSubmit={handleUpdateProduct} className="d-flex align-items-center">
            <div className="col-5">
              <Image src={`../${path}`} alt="..." className="img-fluid d-block mx-auto mb-3"></Image>
            </div>
            <div className="col-7">
              <Form.Group  className="mb-3" controlId="ProductName">
                <Form.Label>Product Name</Form.Label>
                <Form.Control 
                  type="text" 
                  defaultValue={productName}
                  placeholder="ProductName"
                  onChange={(e) => setProductName(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="Description">
                <Form.Label>Description</Form.Label>
                <Form.Control 
                  type="textarea" 
                  defaultValue={description}
                  placeholder="Description"
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="Stocks">
                <Form.Label>Stocks</Form.Label>
                <Form.Control 
                  type="number" 
                  defaultValue={stocks}
                  placeholder="Stocks" 
                  onChange={(e) => setStocks(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="Price">
                <Form.Label>Price</Form.Label>
                <Form.Control 
                  type="number" 
                  defaultValue={price}
                  placeholder="Price" 
                  onChange={(e) => setPrice(e.target.value)}
                />
              </Form.Group>

              <Button variant="primary" type="submit" className="w-100">
                Submit
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  )

}