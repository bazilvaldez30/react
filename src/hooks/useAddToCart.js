import { useState } from "react"
import { useShoppingCart } from "../hooks/useShoppingCartContext"
import { useAuthContext } from "./useAuthContext"


export const useAddToCart = () => {
  const [error, setError] = useState(null);
  const { dispatch } = useShoppingCart();
  const { user } = useAuthContext()
 

  const addToCart = async(productId, quantity) => {
    setError(null)
    console.log(user)
    try{
      // const quantity = 1;
      const response = await fetch(`${process.env.REACT_APP_URI}/cart/addToCart`, {
        method: 'POST',
        body: JSON.stringify({productId, quantity}),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${user.token}`
      }
      })
      
      const json = await response.json()

      if (!response.ok){
        console.log(json.error)
      }

      if (response.ok) { 
         dispatch({type: 'ADD_TO_CART', items: json.product, totalAmmount: json.totalAmountOfAllProducts, numberOfItems: json.product.length})   
      }
    } catch(err){
      console.log(err)
    }
  }

  return {error, addToCart}

}