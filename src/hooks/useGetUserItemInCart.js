import { useAuthContext } from "./useAuthContext"
import { useShoppingCart } from "./useShoppingCartContext"

export const useGetUserItemIncart = () => {

  const { user } = useAuthContext()
  const { dispatch } = useShoppingCart()

  const getItemCart = async() =>{
    
    if(user && !user.isAdmin) {
      const response = await fetch(`${process.env.REACT_APP_URI}/cart/checkYourCart`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${user.token}`
        }
      })
      const json = await response.json()
      if(response.ok){
        dispatch({ 
          type: "SET_CART",
          items: json.product, 
          totalAmmount: json.totalAmountOfAllProducts, 
          numberOfItems: json.product.length 
        })
        
      }
    }
  }

  return {getItemCart}
}