import { useAuthContext } from "./useAuthContext"
import { useShoppingCart } from "./useShoppingCartContext"
import Swal from 'sweetalert2'
import { formatCurrency } from "../utilities/formatCurrency"

export const useBuyNow = () => {

  const { user } = useAuthContext()
  const { dispatch } = useShoppingCart()

  const buyNow = async (productId, quantity) => {
    const response = await fetch(`${process.env.REACT_APP_URI}/cart/buyNow`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "authorization": `Bearer ${user.token}`
      },
      body: JSON.stringify({productId, quantity})
    })
    const json = await response.json()
    const amount = formatCurrency(json.amount)
    if(response.ok){
      // dispatch({type: "CHECKOUT"})
      Swal.fire({
        title: "Order Sucessfuly Placed",
        icon: "success",
        text: `1 Product with a total amount of ${amount}!`
      })
    }
  }

  return { buyNow }
}