import { useAuthContext } from "./useAuthContext"
import { useShoppingCart } from "./useShoppingCartContext"
import Swal from 'sweetalert2'

export const useCheckOut = () => {

  const { user } = useAuthContext()
  const { dispatch } = useShoppingCart()

  const checkOut = async () => {
    const response = await fetch(`${process.env.REACT_APP_URI}/cart/checkOutOrder`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "authorization": `Bearer ${user.token}`
      }
    })
    const json = await response.json()
    if(response.ok){
      dispatch({type: "CHECKOUT"})
      Swal.fire({
        title: "Order Sucessfuly",
        icon: "success",
        text: `${json.msg}\nThank you for shopping with us!`
      })
      console.log(json.msg)
    }
  }

  return { checkOut }
}