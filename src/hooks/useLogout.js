import { useAuthContext } from "./useAuthContext"
import { useShoppingCart } from "./useShoppingCartContext"

// import { useProductsContext } from "./useProductsContext"

export const useLogout = () => {
  const { dispatch } = useAuthContext()
  const { dispatch: cartDispatch } = useShoppingCart()
  
  // const { dispatch: workoutsDispatch } = useProductsContext()

  const logout = () => {
    // remove user from storage
    localStorage.removeItem('user')

    // dispatch logout action
    dispatch({type: 'LOGOUT'})
    cartDispatch({type: 'LOG_OUT'})
    // workoutsDispatch({type: 'SET_PRODUCTS', payload: null})
  }

  return {logout}

}