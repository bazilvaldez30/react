import { useState } from "react";
import { useAuthContext } from "./useAuthContext";
import { useShoppingCart } from "./useShoppingCartContext";

export const useLogin = () => {

  const [error, setError] = useState(null)
  const [loading, setIsLoading] = useState(null)
  const { dispatch } = useAuthContext()
  
  const { getItemCart } = useShoppingCart()

  const login = async (email, password) => {
    setIsLoading(true)
    setError(null)

    const response = await fetch(`${process.env.REACT_APP_URI}/users/login`, {
      method: 'POST',
      body: JSON.stringify({email, password}),
      headers: { 
        'Content-Type': 'application/json',
      }
    })
    const json = await response.json()

    if (!response.ok) {
      setIsLoading(false)
      setError(json.error)
    }
    if (response.ok) {
      // save the user to local storage
      localStorage.setItem('user', JSON.stringify(json))
      // update the auth context
      await dispatch({type: 'LOGIN', payload: json})
      // await getItemCart()
      setIsLoading(false)
    }
     
  } 

  return { login, setIsLoading, error}
}