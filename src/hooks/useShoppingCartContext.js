import { useContext } from "react";
import { ShoppingCartContext } from "../context/ShoppingCartContext";


export function useShoppingCart() {
  
 const context = useContext(ShoppingCartContext)

 if (!context) {
  throw Error('useShoppingCartContext must be used inside an ShoppingCartContextProvider')
}

return context
}

