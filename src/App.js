import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import { useAuthContext } from './hooks/useAuthContext';

// pages & components
import Home from './pages/Home'
import Signup from './pages/Signup'
import Login from './pages/Login';
import Products from './pages/Products';
import MyNavbar from './components/Navbar'
import AddProducts from './pages/AddProducts'
import Cart from './pages/Cart';
import PageNotFound from './pages/PageNotFound';
import EditProductView from './pages/EditProductView';
import ProductView from './pages/ProductView';


function App() {
  const { user } = useAuthContext()
  return (
    
      <div className="App">
        <BrowserRouter>
          <MyNavbar />
          
          <div className="pages">
            <Routes>
              <Route 
                path="*"
                element={<PageNotFound />}
              />
              <Route 
                path="/"
                element={<Home />}
              />
              <Route 
                path="/signup"
                element={!user ? <Signup /> : <Navigate to="/"/>}
              />
              <Route 
                path="/login"
                element={!user ? <Login /> : <Navigate to="/"/>}
              />
              <Route 
                path="/products"
                element={<Products />}
              />
              <Route 
                path="/products/:productId"
                element={<ProductView />}
              />
              <Route 
                path="/addproducts"
                element={user && user.isAdmin ? <AddProducts /> : <Navigate to="/"/>}
              />
              <Route 
                path="/editproducts/:productId"
                element={user && user.isAdmin ? <EditProductView/> : <Navigate to="/"/>}
              />
              <Route 
                path="/cart"
                element={<Cart />}
              />
            </Routes>
          </div>
        </BrowserRouter>
      </div>
    
  );
}

export default App;
