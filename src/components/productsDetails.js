import { Link, Navigate } from "react-router-dom"
import { useProductsContext } from "../hooks/useProductsContext"
import { Button, Card } from "react-bootstrap"
import { useAddToCart } from "../hooks/useAddToCart"
import { useAuthContext } from "../hooks/useAuthContext"
import { formatCurrency } from "../utilities/formatCurrency"




const ProductDetails = ({ product }) => {

    const { user } = useAuthContext()

    const { error, addToCart } = useAddToCart();

    const { dispatch } = useProductsContext();

    const quantity = 1;

   
    const handleClick =  async (e) => {
        e.preventDefault()
        console.log(product._id)
        await addToCart(product._id, quantity)
    }

    const handleClickDelete = async (e) => {
        e.preventDefault()

        const response = await fetch(`${process.env.REACT_APP_URI}/products/deleteProduct`, {
            method: 'DELETE',
            headers: {
                "Content-Type" : "application/json",
                "authorization" : `Bearer ${user.token}`
            },
            body: JSON.stringify({_id: product._id})
        })
        const json = await response.json()
        if(response.ok){
            dispatch({type: "SET_PRODUCTS", payload: json})
        }
    }
    
    const handleClickArchived = async (e) => {

        e.preventDefault()

        const response = await fetch(`${process.env.REACT_APP_URI}/products/archiveProducts/` + product._id, {
            method: 'PATCH',
            headers: {
                "Content-Type": "application/json",
                'authorization': `Bearer ${user.token}`
            }
        })
        const json = await response.json()
        if(response.ok){
            dispatch({type: "SET_PRODUCTS", payload: json})
        }
        console.log("notadmin")
        
    }

    return (
        
    <div className="col-lg-3 col-md-6 mb-0 mb-lg-3">
        <Card className="product-card-body card rounded shadow-sm border-0">
            <Card.Body className="p-0">
                <div>
                    <div className="img-area">
                        <div className="img-div d-flex justify-content-around align-items-center">
                            <img src={`../${product.path}`} alt="" className="product-img product-main img-fluid mb-md-3 mb-0"/>
                            <div className="buttons-hover col-12">
                            {user ? 
                                <>
                                    {user.isAdmin ? 
                                        <>
                                            <Button onClick={handleClickDelete} className="hover w-50">Delete</Button>
                                            <Button as={Link} to={`/editproducts/${product._id}`}  className="hover w-50 mt-2">Edit</Button>
                                        </>
                                        :
                                        <Button as={Link} to={`/products/${product._id}`}  className="hover w-50">View Details</Button>
                                    }
                                </>
                                :
                                <Button as={Link} to={`/products/${product._id}`}  className="hover w-50">View Details</Button>
                            }
                            {user && (
                                <Button hidden={user.isAdmin} onClick={handleClick} className="hover w-50 mt-2">Add to cart</Button>
                            )}
                            {user && (
                                <>
                                {user.isAdmin && product.isActive && (
                                    <Button onClick={handleClickArchived} className="hover w-50 mt-2">Archived</Button>
                                )}
                                {user.isAdmin && !product.isActive && (
                                    <Button onClick={handleClickArchived} className="hover w-50 mt-2" disabled={product.stocks == 0}>Unarchived</Button>
                                )}
                                </>
                            )}
                            {!user && (
                                <Button as={Link} to="/login" className="hover w-50 mt-2">Add to cart</Button>
                            )}
                                    
                            </div>
                        </div>
                        {/* <div className="img-text">
                            <div className="content d-flex flex-column justify-content-center align-items-center">
                            {user ? 
                                <>
                                    {user.isAdmin ? 
                                        <>
                                            <Button onClick={handleClickDelete} className="hover w-50">Delete Products</Button>
                                            <Button as={Link} to={`/editproducts/${product._id}`}  className="hover w-50 mt-2">Edit Products</Button>
                                        </>
                                        :
                                        <Button as={Link} to={`/products/${product._id}`}  className="hover w-50">View Details</Button>
                                    }
                                </>
                                :
                                <Button as={Link} to={`/products/${product._id}`}  className="hover w-50">View Details</Button>
                            }
                            {user && (
                                <Button hidden={user.isAdmin} onClick={handleClick} className="hover w-50 mt-2">Add to cart</Button>
                            )}
                            {user && (
                                <>
                                {user.isAdmin && product.isActive && (
                                    <Button onClick={handleClickArchived} className="hover w-50 mt-2">Archived Products</Button>
                                )}
                                {user.isAdmin && !product.isActive && (
                                    <Button onClick={handleClickArchived} className="hover w-50 mt-2">Unarchived Products</Button>
                                )}
                                </>
                            )}
                            {!user && (
                                <Button as={Link} to="/login" className="hover w-50 mt-2">Add to cart</Button>
                            )}
                                    
                            </div>
                        </div> */}
                    </div>
                </div>
                <div className="text-center">
                    <p className="small text-muted font-italic">{product.name}  --- <strong>{formatCurrency(product.price)}</strong></p>
                </div>
            </Card.Body>
            {error && <div className="error">{error}</div>}
        </Card>
    </div>

    )
}

export default ProductDetails