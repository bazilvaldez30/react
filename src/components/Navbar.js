import { Link } from 'react-router-dom'
import { useLogout } from '../hooks/useLogout'
import { useAuthContext } from '../hooks/useAuthContext'
import { Nav, Navbar, Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom'
import { useShoppingCart } from '../hooks/useShoppingCartContext';
import { useEffect } from 'react';


const MyNavbar = () => {

    const { logout } = useLogout()
    const { user } = useAuthContext()
    const { handleShow , numberOfItems } = useShoppingCart()

    const handleClick = (e) => {
        logout()
    }
    return (
    <>
    <Navbar className='navbar pl-5 pr-5 sticky-top' bg="light" expand="lg">
        <Navbar.Brand as={NavLink} to="/">
            <img className="trendy-logo" src="../images/trendyrtw.jpg"/>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse  id="basic-navbar-nav">
          <Nav className="Nav offset-md-4 col-md-8 justify-content-end align-items-center">
            {user && (
                <>
                    {user.isAdmin && (
                        <>
                        <Nav.Link as={NavLink} to="/addproducts">Add Products</Nav.Link>
                        </>
                    )}
                </>
            )}
            <Nav.Link as={NavLink} to="/products" className="display-5">Products</Nav.Link>
            {!user ? 
            <>
                <Nav.Link as={NavLink} to="/signup">Register</Nav.Link>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            </>
            :
            <>
            
                <Link to="/" className="material-symbols-outlined text-decoration-none text-secondary ">
                    account_circle
                </Link>
            
                <Link to="/login" onClick={handleClick} className="d-flex align-items-center text-decoration-none">
                    <span className="material-symbols-outlined text-dark">
                        logout
                    </span>
                </Link>
            </>
            }
            {user ?
                <Button hidden={user && user.isAdmin} onClick={() => handleShow()} style={{width: "3rem", height: "3rem", position: "relative"}} variant="outline-primary" className='rounded-circle'>
                        <>
                        <span className="material-symbols-outlined">shopping_cart</span> 
                        <div className='cart-number rounded-circle bg-danger d-flex justify-content-center align-items-center'>
                            {numberOfItems}
                        </div>
                        </>
                </Button>
                :
                <Button as={Link} to="/login" style={{width: "3rem", height: "3rem", position: "relative"}} variant="outline-primary" className='rounded-circle'>
                        <>
                        <span className="material-symbols-outlined mt-1">shopping_cart</span> 
                        <div className='cart-number rounded-circle bg-danger d-flex justify-content-center align-items-center'>
                            {numberOfItems}
                        </div>
                        </>
                </Button>
            }
            
          </Nav> 
        </Navbar.Collapse>
    </Navbar>

    </>
    )
}

export default MyNavbar