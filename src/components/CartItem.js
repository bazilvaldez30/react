import { useEffect, useState } from "react"
import { NavItem, Stack, Button, Image } from "react-bootstrap"
import { Link } from "react-router-dom"
import { useAuthContext } from "../hooks/useAuthContext"
import { useShoppingCart } from "../hooks/useShoppingCartContext"
import { formatCurrency } from "../utilities/formatCurrency"

const CartItem = ({product}) => {
  
  const [ productName, setProductName ] = useState("")
  const [ path, setPath ] = useState("")
  const [ price, setPrice ] = useState(0)
  const [ quantity, setQuantity ] = useState(0)
  const { user } = useAuthContext()
  const { dispatch, items, totalAmmount } = useShoppingCart()
  

  useEffect(() => {
    setProductName(product.productName)
    setQuantity(product.quantity)
    setPrice(product.price)
    setPath(product.path)
  }, [])
  
  const handleAddQuantity = async (e) => {
    console.log(product.path)
    console.log(productName)
    e.preventDefault()
    console.log(product._id)
    const response = await fetch(`${process.env.REACT_APP_URI}/cart/editProductQuantity`, {
      method: 'PUT',
      body: JSON.stringify({productId: product.productId}),
      headers: {
        'Content-Type': 'application/json',
        'authorization': `Bearer ${user.token}`
      }
    })
    
    const json = await response.json()
    
    if(response.ok){

      setQuantity(quantity +1)

      let initialItems = items;
      let updatedItem = []
      let updatedTotalAmount = 0;
      initialItems.forEach(item => {
          if (item.productId === product.productId){
             item.quantity += 1
             updatedTotalAmount = totalAmmount + item.price
            }
            updatedItem.push(item)
          }
        )

      dispatch({type: "CHANGE_QUANTITY", items: updatedItem, totalAmmount: updatedTotalAmount})
      
    }
    if(!response.ok){
      console.log(json.error)
    }
  }

  const handleMinusQuantity = async (e) => {
    e.preventDefault()
   
    const response = await fetch(`${process.env.REACT_APP_URI}/cart/decreaseProductQuantity`, {
      method: 'PUT',
      body: JSON.stringify({productId: product.productId}),
      headers: {
        'Content-Type': 'application/json',
        'authorization': `Bearer ${user.token}`
      }
    })
    
    const json = await response.json()
    
    if(response.ok){
      setQuantity(quantity -1)

      let initialItems = items;
      let updatedItem = []
      let updatedTotalAmount = 0;
      let i = 0;
      console.log(updatedTotalAmount)
      initialItems.forEach(item => {
          i++
          if (item.productId === product.productId){
             item.quantity -= 1;
             updatedTotalAmount = totalAmmount - item.price
            }
            
          updatedItem.push(item) 
          
          }     
        )
        console.log(updatedTotalAmount)
    
        
      

      dispatch({ type: "CHANGE_QUANTITY", items: updatedItem, totalAmmount: updatedTotalAmount })
      
    }
    if(!response.ok){
      console.log(json.error)
    }
  }

  const handleUpdateQuantityByInput = async(e) => {
    e.preventDefault()

    const response = await fetch(`${process.env.REACT_APP_URI}/cart/updateByInputQuantity`, {
      method: 'PUT',
      body: JSON.stringify({productId: product.productId, quantity}),
      headers: {
        'Content-Type': 'application/json',
        'authorization': `Bearer ${user.token}`
      }
    })
    
    const json = await response.json()
    console.log(json)
    if(!response.ok){
      console.log(json.error)
    }

    if(response.ok){
      dispatch({ 
        type: "SET_CART",
        items: json.product, 
        totalAmmount: json.totalAmountOfAllProducts, 
        numberOfItems: json.product.length 
      })
    }
  }

  const handleDeleteProduct = async (e) => {
    e.preventDefault()

    const response = await fetch(`${process.env.REACT_APP_URI}/cart/removeProduct/${product.productId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'authorization': `Bearer ${user.token}`
      }
    })

    const json = await response.json()
    if(response.ok){
      dispatch({type: 'SET_CART', items: json.product, totalAmmount: json.totalAmountOfAllProducts, numberOfItems: json.product.length}) 
    }

  }


  return (
  
  <Stack direction="horizontal" gap={2} className="usercart-items d-flex align-items-center">
    <Image as={Link} to="/dasdsa" src={`../${path}`} alt="" className="cart-img img-fluid"/>
    <div className="me-auto col-4">
      <div>
        <strong style={{ fontSize: ".75rem" }}>{productName}{" "}</strong>
        {quantity > 1 && (
          <span  style={{fontSize: ".65rem"}}>
            x{quantity}
          </span>
        )}
      </div>
      <div  style={{ fontSize: ".75rem" }}>
          {formatCurrency(price)}
      </div>
    </div>
    <div className="text-center">
      <div className="d-flex flex-column justify-content-around align-items-center">
        <button onClick={handleDeleteProduct} className="material-symbols-outlined mb-2" style={{fontSize: "1rem"}}>delete</button>
        <span>{formatCurrency(price * quantity)}</span>
      </div>
      <div className="mt-2 d-flex justify-content-center align-items-center">
          {quantity === 1 ? 
            <button className="plus-minus-btn" onClick={handleDeleteProduct}>-</button> 
            :
            <button className="plus-minus-btn" onClick={handleMinusQuantity}>-</button>
          }
        
          <input type="number" min="1" max="50" className="cart-quantity col-4 text-center" value={quantity} onChange={(e) => setQuantity(e.target.value)} onBlur={(e) => {if(quantity < 1){return handleDeleteProduct(e)}handleUpdateQuantityByInput(e)}}/>
        <button className="plus-minus-btn" onClick={handleAddQuantity}>+</button>
      </div>
    </div>
  </Stack>
  
  )
  
}

export default CartItem