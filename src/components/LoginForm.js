import { useState } from "react"
import { Form, Button, FloatingLabel } from "react-bootstrap";
import { useLogin } from "../hooks/useLogin";
import { Link } from "react-router-dom"
import { useShoppingCart } from "../hooks/useShoppingCartContext";


const LoginForm = () => {
    
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { login, error, isLoading } = useLogin()
    // const [error, setError] = useState(null)
    // const [emptyFields, setEmptyFields] = useState([])

    const { getItemCart } = useShoppingCart()

    const handleCreateAccount = async (e) => {
        e.preventDefault()

        await login(email, password)
        
        getItemCart()
    }

    return(
    
        <Form className="login" onSubmit={handleCreateAccount}>
            <h5 className="fw-normal mb-3 pb-3">Sign into your account</h5>
            <Form.Group>
                <FloatingLabel label="Email address" className={email && 'filled'}>
                <Form.Control 
                    type="email" 
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                    className="form-control form-control-lg"
                    placeholder="name@example.com"
                    // aria-describedby="emailHelp"
                    // className={emptyFields.includes('email') ? 'error' : ''}
                />
                </FloatingLabel>
                
                {/* <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small> */}
            </Form.Group>
            
            <Form.Group className="mt-2">
                  <FloatingLabel label="Password" className={password && 'filled'}> 
                  <Form.Control
                      type="password" 
                      onChange={(e) => setPassword(e.target.value)}
                      value={password}
                      className="form-control form-control-lg"
                      placeholder="Password"
                      // className={emptyFields.includes('password') ? 'error' : ''}
                  /> 
                  </FloatingLabel>
            </Form.Group>
            
            <Button disabled={isLoading} type="submit" className="btn btn-dark btn-lg btn-block mt-4">Sign In</Button>
            {error && <div className="error">{error}</div>}
            <Form.Group className="mt-3">
              <Link to="/products" className="small text-muted text-decoration-none">Forgot password?</Link>
              <p className="mb-5 pb-lg-2">Don't have an account? <Link to="/signup" className="text-decoration-none">Register here</Link></p>
            </Form.Group>
        </Form>
    
    )
}

export default LoginForm