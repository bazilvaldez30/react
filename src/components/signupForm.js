 import { useState } from "react"
import { Form, Button } from "react-bootstrap";
 import { useSignup } from "../hooks/useSignup";

const SignupForm = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmpassword, setConfirmPassword] = useState('');
    const {signup, error, isLoading} = useSignup()
   

    const handleCreateAccount = async (e) => {
        e.preventDefault()

        await signup(email, password, confirmpassword)

        
    }

    return(
        <Form className="createAccount" onSubmit={handleCreateAccount}>
            <h4 className="mb-4">Sign Up</h4>
            <Form.Group className="mb-2 mt-2">
                <Form.Label htmlFor="email">Email address *</Form.Label>
                <Form.Control 
                    type="email" 
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                    id="email" 
                    aria-describedby="emailHelp"
                    // className={emptyFields.includes('email') ? 'error' : ''}
                />
                <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
            </Form.Group>
            <Form.Group className="mb-2">
                <Form.Label htmlFor="password">Password *</Form.Label>
                <Form.Control 
                    type="password" 
                    onChange={(e) => setPassword(e.target.value)}
                    value={password}
                    id="password"
                    // className={emptyFields.includes('password') ? 'error' : ''}
                />
            </Form.Group>
            <Form.Group className="mb-4">
                <Form.Label htmlFor="confirmpassword">Confirm Password *</Form.Label>
                <Form.Control 
                    type="password"
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    value={confirmpassword}
                    id="confirmpassword"
                    // className={emptyFields.includes('confirmPassword') ? 'error' : ''}
                />
            </Form.Group>
            <Button disabled={isLoading} type="submit" className="btn btn-dark mb-2 btn-block p-2 display-4">Create account</Button>
            {error && <div className="error">{error}</div>}
        </Form>
    )
}

export default SignupForm