import { createContext, useState, useReducer, useEffect } from "react";
import { useAuthContext } from "../hooks/useAuthContext";
import Cart from "../pages/Cart";

export const ShoppingCartContext = createContext()

const user = JSON.parse(localStorage.getItem('user'))

 const cartReducer = (state, action) => {
  switch(action.type){
    case 'SET_CART':
      return {
        items: action.items,
        totalAmmount: action.totalAmmount,
        numberOfItems: action.numberOfItems
      }
    case 'ADD_TO_CART':
      return {
        items: action.items,
        totalAmmount: action.totalAmmount,
        numberOfItems: action.numberOfItems
      }
    case 'CHANGE_QUANTITY':
      return {
        items: action.items,
        totalAmmount: action.totalAmmount,
        numberOfItems: state.numberOfItems
      }
    case 'CHECKOUT':
      return {
        items: [],
        totalAmmount: 0,
        numberOfItems: 0
      }
    case 'LOG_OUT':
      return{
        items: [],
        totalAmmount: 0,
        numberOfItems: 0
      }
    // case 'REMOVE_FROM_CART':
    //   return {
    //     userCart: state.userCart.filter((c) => c._productId !== action.payload._productId)
    //   }
    default: 
      return state
  }
}


export async function getUserCart() {
  
  if(user && !user.isAdmin) {
    const response = await fetch(`${process.env.REACT_APP_URI}/cart/checkYourCart`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.token}`
      }
    })
    const json = await response.json()
    if(response.ok){
      return json
    }
  }
}
      
    


export function ShoppingCartProvider({ children }) {

  const [show, setShow] = useState(false);
  const { user: globalUser } = useAuthContext()
  function handleClose() { setShow(false); }
  function handleShow() { setShow(true); }
  
  const [state, dispatch] = useReducer(cartReducer, {
    items: [],
    totalAmmount: 0,
    numberOfItems: 0
  })

  useEffect(() => {
    
    if(globalUser){
      getItemCart()
    }
    
  }, [globalUser])


  const getItemCart = async () => {
    
    if(user && !user.isAdmin) {
      const userCart = await getUserCart();
      dispatch({ 
        type: "SET_CART",
        items: userCart.product, 
        totalAmmount: userCart.totalAmountOfAllProducts, 
        numberOfItems: userCart.product.length 
      })
    }
  }

  

  // function getItemQuantity(productId) {
  //   return userCart.find(item => item.productId === productId)?.quantity || 0
  // }
  
  // function increaseCartQuantity(product) {

      
  //        state.items.map(item => {
  //         if (item.productId === productId){
  //           return { ...item, quantity: item.quantity +1}
  //         }
  //       })
  //      }
  //   dispatch({})
  // }


  // function increaseCartQuantity(product) {
  //   const { productId, quantity, stocks } = product
  //   setUserCart(currItems => {
  //     if (currItems.product.find(item => item.productId === productId) == null){
  //       console.log("added new product to cart")
  //        return [...currItems.product, {productId, quantity: quantity+ 1, stocks }]
  //     }
  //      else {
  //       return currItems.product.map(item => {
  //         if (item.productId === productId){
  //           return { ...item, quantity: item.quantity +1}
  //         }
  //       })
  //      }
  //   })
  // }
  //   console.log(userCart)
  

  // function decreaseCartQuantity(productId) {
  //   setUserCart(currItems => {
  //     if (currItems.find(item => item.productId === productId)?.quantity === 1){
  //       return currItems.filter(item => item.productId !== productId)
  //     } 
  //     else {
  //       return currItems.map(item => {
  //         if (item.productId === productId){
  //           return { ...item, quantity: item.quantity - 1}
  //         }
  //       })
  //     }
  //   })
  // }

  // function removeFromCart(productId) {
  //   setUserCart(currItems => {
  //     return currItems.filter(item => item.productId !== productId)
  //   })
  // }


  return (
    <ShoppingCartContext.Provider value={{...state, dispatch, handleShow, handleClose, show, setShow, getItemCart}}>
      { children }
      <Cart/>
    </ShoppingCartContext.Provider>
  )
}